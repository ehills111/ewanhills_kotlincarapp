package com.example.myapplication555


import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    internal var dbHelper = DatabaseHelper(this)


    //####################################################################################################################################//
    /**
     * Toast Message
     * https://stackoverflow.com/questions/36826004/how-do-you-display-a-toast-using-kotlin-on-android
     * Android Toast is used to display a sort time notification to the user without affecting the user interaction with UI.
     * The message displayed using
     * Toast class displays quickly, and it disappears after some time. The message in the Toast can be of type text, image or both.
     */
    fun showToast(text: String) {
        Toast.makeText(this@MainActivity, text, Toast.LENGTH_LONG).show()
    }

    //####################################################################################################################################//

    /**
     * Each time a value has been added it will clear the fields that had values present
     */
    fun clearEditTexts() {
        carType.setText("")
        modificationsMade.setText("")
        bhpAmount.setText("")
        carId.setText("")
        torqueTotal.setText("")
        CostOfModifications.setText("")
        acceleration.setText("")

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        handleInserts()
        handleUpdates()
        handleDeletes()
        handleViewing()
        handleShare()
    }

fun handleShare(){
    share.setOnClickListener {
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        val shareBody = "https://play.google.com/store/apps/details?id=air.com.A3dtuning.Tuning3D&gl=IE"
        val shareSub = "https://play.google.com/store/apps/details?id=air.com.A3dtuning.Tuning3D&gl=IE"
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, shareSub)
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        startActivity(Intent.createChooser(sharingIntent, "Share using"))
    }
}
    //####################################################################################################################################//
    /**
     * Alerts the user to changes that have taken place ie delete
     */
    fun showDialog(title: String, Message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(true)
        builder.setTitle(title)
        builder.setMessage(Message)
        builder.show()
    }
    //####################################################################################################################################//
    /**
     * When our handleInserts button is clicked.
     */
    fun handleInserts() {
        insertBtn.setOnClickListener {
            try {
                dbHelper.insertData(carType.text.toString(), modificationsMade.text.toString(), bhpAmount.text.toString(),
                        torqueTotal.text.toString(), CostOfModifications.text.toString(), acceleration.text.toString())
                clearEditTexts()
            } catch (e: Exception) {
                e.printStackTrace()
                showToast(e.message.toString())
            }
        }
    }


    //####################################################################################################################################//
    /**
     * When our handleUpdates data button is clicked
     */
    fun handleUpdates() {
        updateBtn.setOnClickListener {
            try {
                val isUpdate = dbHelper.updateData(carId.text.toString(),
                        carType.text.toString(),
                        modificationsMade.text.toString(),
                        bhpAmount.text.toString(),
                        torqueTotal.text.toString(),
                        CostOfModifications.text.toString(),
                        acceleration.text.toString())
                if (isUpdate == true)
                    showToast("Data Updated Successfully")
                else
                    showToast("Data Not Updated")
            } catch (e: Exception) {
                e.printStackTrace()
                showToast(e.message.toString())
            }
        }
    }

    //####################################################################################################################################//
    /**
     * When our handleDeletes button is clicked
     */
    fun handleDeletes() {
        deleteBtn.setOnClickListener {
            try {
                dbHelper.deleteData(carId.text.toString())
                clearEditTexts()
            } catch (e: Exception) {
                e.printStackTrace()
                showToast(e.message.toString())
            }
        }
    }


    //####################################################################################################################################//
    /**
     * When our View All is clicked
     */
    fun handleViewing() {
        viewBtn.setOnClickListener(
                View.OnClickListener {
                    val res = dbHelper.allData
                    if (res.count == 0) {
                        showDialog("Error", "No Data Found")
                        return@OnClickListener
                    }

                    val buffer = StringBuffer()
                    while (res.moveToNext()) {
                        buffer.append("CARID :" + res.getString(0) + "\n")
                        buffer.append("CARNAMETYPE :" + res.getString(2) + "\n")
                        buffer.append("MODIFICATIONSMADE :" + res.getString(3) + "\n")
                        buffer.append("BHPAMOUNT :" + res.getString(4) + "\n")
                        buffer.append("TORQUETOTAL :" + res.getString(5) + "\n")
                        buffer.append("COSTOFMODIFICATIONS :" + res.getString(6) + "\n")
                        buffer.append("ACCELERATION :" + res.getString(7) + "\n\n")

                    }
                    showDialog("Data Listing", buffer.toString())
                }
        )
    }

}